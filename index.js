// This code will help us access contents of express module/package
	// a "module" is a software component or a part of a program that containes one or more routines

// It also allows us to access methods and functions that we will use to easily create an app/server
// We store our express module to a variable so we could easily access its keywords, functions, and methods.
const express = require ("express");


// This code creates an application using express / a.k.a. express application
	// Our app is our server
const app = express();


// For our application servers to run, we need a port to listen to
const port = 3000;


// v This is so that our app could read json data
app.use(express.json());


// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applyhing the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// This route expects to receive a GET request at the URI/endoiunt "/hello"
app.get("/hello", (request, response)=>{
	response.send("GET method success. \nHello from /hello endpoint!");
});


app.post("/hello", (request, response)=>{
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


let users=[];

app.post("/signup", (request, response)=>{
	if(request.body.username !== "" && request.body.password !== "" ){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}
	else{
		response.send("Please input BOTH username and password");
	}
});

//Syntax:
/*app.httpMethod("/endpoint", (request, response)=>{

	// code block

});*/


// We use the PUT method to update a document 
app.put("/change-password", (request, response)=>{

	// We create a variable where we will store our response
	let message;

	// The purpose of our loop is to check our per element in users array if it matches our request.body.username
	// initialization // condition // change of value/index
	for(let i=0; i < users.length; i++){

		// We check if the user is existing in our users array
		if(request.body.username==users[i].username){

			// update an element object's password based on the input on the body
			users[i].password=request.body.password

			// We reassign what message we would like to receive as a response.
			message=`User ${request.body.username}'s password has been updated`;
			break;
		}

		else{

			// If there is no match, we would like to receive a message that "user does not exist".
			message="User does not exist."
		}

	}

	// We display our updated array
	console.log(users);

	// We display our response based if there is a match (successfully updated the password) or if there is no match (User does not exist)
	response.send(message);

});

// 1
// Create a route that expects GET request at the URI/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
// Code below...
app.get("/home", (request, response)=>{
	response.send("Welcome to the homepage!");
});

// 2
// Create a route that expects GET request at the URI/endpoint "/users"
// Response that will be received is a collection of users
app.get("/users", (request, response)=>{
	response.send(users);
});

// 3
// Create a route that expects DELETE request at the URI/endpoint "/delete-user"
// The program should check if the user is existing before deleting
// Response that will be received is the updated collection of users
app.delete("/delete-user", (request, response) => {
	const username = request.body.username
    for(let i = 0; i < users.length; i++) {
        if(username == users[i].username) {
            users.splice(i, 1);
            break;
        }
    }
    response.send(users);
});

// 4
// Save all the successful request tabs and export the collection
// Update your Gitlab repo and link it to boodle




//Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, ()=> console.log(`Server running at port ${port}`))

